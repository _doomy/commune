//! Common pins when using the NRF24L01 RF transceiver. You can use any
//! compatible pins, but I like to keep them the same for easier connections.
//! Here you can find definitions for both the `stm32f1xx` and the `stm32f4xx`,
//! although they both may work for many other 1xx and 4xx series respectively.

// Rust Sensor Network addresses
pub const TX_ADDRESS: [u8; 5] = *b"TXRSN";
pub const RX_ADDRESS: [u8; 5] = *b"RXRSN";

/// Where sensors should address their data to
pub const CONTROLLER_ID: u8 = 0x0;

#[cfg(feature = "stm32f1xx")]
pub mod pins {
    use core::convert::Infallible;
    use hal::gpio::{gpiob::*, Alternate, Floating, Input, Output, PullUp, PushPull};
    use hal::spi::{Spi, Spi2NoRemap};
    use nrf::NRF24L01;
    use stm32f1::stm32f103::SPI2;
    use stm32f1xx_hal as hal;

    /// Chip enable
    pub type Ce = PB0<Output<PushPull>>;
    ///
    pub type Csn = PB1<Output<PushPull>>;
    /// Interrupt
    pub type Irq = PB10<Input<PullUp>>;
    /// SPI pins
    pub type NrfSpiPins = (
        PB13<Alternate<PushPull>>,
        PB14<Input<Floating>>,
        PB15<Alternate<PushPull>>,
    );

    pub type NrfSpi = Spi<SPI2, Spi2NoRemap, NrfSpiPins>;

    /// The complete NRF device driver
    pub type Nrf = NRF24L01<Infallible, Ce, Csn, NrfSpi>;
}

#[cfg(feature = "stm32f4xx")]
pub mod pins {
    use core::convert::Infallible;
    use hal::{
        gpio::{gpiob::*, Alternate, Floating, Input, Output, PullUp, PushPull},
        spi::Spi,
        stm32::SPI1,
    };
    use nrf::NRF24L01;
    use stm32f4xx_hal as hal;

    /// Chip enable
    pub type Ce = PB0<Output<PushPull>>;
    ///
    pub type Csn = PB1<Output<PushPull>>;
    /// Interrupt
    pub type Irq = PB2<Input<PullUp>>;
    /// SPI pins
    pub type NrfSpiPins = (
        PB3<Alternate<PushPull>>,
        PB4<Input<Floating>>,
        PB5<Alternate<PushPull>>,
    );

    pub type NrfSpi = Spi<SPI1, NrfSpiPins>;

    /// The complete NRF device driver
    pub type Nrf = NRF24L01<Infallible, Ce, Csn, NrfSpi>;
}
