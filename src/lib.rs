#![no_std]
#![feature(const_in_array_repeat_expressions)]
#![feature(const_fn)]

mod device;
mod frame;
#[cfg(feature = "radio")]
pub mod radio;

pub use crate::device::Device;
pub use crate::frame::{Frame, MultipartStatus};

pub const BUFFER_TOTAL_SIZE_BYTES: usize = BUFFER_TOTAL_FRAMES * FRAME_SIZE_BYTES;
/// Total size in bytes for our data buffer.
pub const BUFFER_DATA_TOTAL_SIZE_BYTES: usize = BUFFER_TOTAL_FRAMES * DATA_SIZE_BYTES;
/// Max number of frames we can send for a multipart
pub const BUFFER_TOTAL_FRAMES: usize = 64;
/// The size of our packet in bytes. This is always 32 for the NRF24L01.
pub const FRAME_SIZE_BYTES: usize = 32;
/// The amount of bytes per frame data actually takes up. The rest  is metadata.
pub const DATA_SIZE_BYTES: usize = 24;
/// The amount of bytes per frame that metadata occupies, calculated as
/// `PACKET_SIZE - DATA_SIZE`
pub const METADATA_SIZE_BYTES: usize = FRAME_SIZE_BYTES - DATA_SIZE_BYTES;
/// While each device can act independently, usually a "hub" of some kind is
/// available to aggregate everything. The ID of that hub is, by standard, 0.
pub const CONTROLLER_ID: usize = 0x0;

fn round_up_int_div(top: usize, bot: usize) -> usize {
    (top + (bot - 1)) / bot
}

/// Error handling
#[derive(Debug)]
pub enum Error {
    Generic(&'static str),
    OutOfBounds,
    Cbor(&'static str),
    U16IntoBytes,
    SerializationGeneral,
    DeserializationGeneral,
    InvalidScratchBuffer,
}

#[cfg(test)]
mod tests {
    use crate::frame::Frame;

    use super::*;
    use serde::{Deserialize, Serialize};

    /// Represents a generic data structure that can be deserialized and sent
    #[derive(Serialize, Deserialize, Debug, PartialEq)]
    pub enum TestData<'a> {
        None,
        Text(&'a str),
    }

    impl<'a> Default for TestData<'a> {
        fn default() -> Self {
            Self::Text("this is a really long message that is sure to take multiple frames to send! right?")
        }
    }

    impl<'a> TestData<'a> {
        pub fn default_serialized() -> [u8; 90] {
            let r: [u8; 90] = [
                161, 100, 84, 101, 120, 116, 120, 82, 116, 104, 105, 115, 32, 105, 115, 32, 97, 32,
                114, 101, 97, 108, 108, 121, 32, 108, 111, 110, 103, 32, 109, 101, 115, 115, 97,
                103, 101, 32, 116, 104, 97, 116, 32, 105, 115, 32, 115, 117, 114, 101, 32, 116,
                111, 32, 116, 97, 107, 101, 32, 109, 117, 108, 116, 105, 112, 108, 101, 32, 102,
                114, 97, 109, 101, 115, 32, 116, 111, 32, 115, 101, 110, 100, 33, 32, 114, 105,
                103, 104, 116, 63,
            ];
            r
        }

        pub fn default_frames() -> [Frame; 4] {
            [
                Frame([
                    0x0, 0x1, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 161, 100, 84, 101, 120, 116, 120, 82,
                    116, 104, 105, 115, 32, 105, 115, 32, 97, 32, 114, 101, 97, 108, 108, 121,
                ]),
                Frame([
                    0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 32, 108, 111, 110, 103, 32, 109, 101,
                    115, 115, 97, 103, 101, 32, 116, 104, 97, 116, 32, 105, 115, 32, 115, 117,
                ]),
                Frame([
                    0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 114, 101, 32, 116, 111, 32, 116, 97,
                    107, 101, 32, 109, 117, 108, 116, 105, 112, 108, 101, 32, 102, 114, 97, 109,
                ]),
                Frame([
                    0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 101, 115, 32, 116, 111, 32, 115, 101,
                    110, 100, 33, 32, 114, 105, 103, 104, 116, 63, 0, 0, 0, 0, 0, 0,
                ]),
            ]
        }
    }

    #[test]
    fn correct_round_up_int_div() {
        assert_eq!(round_up_int_div(7, 2), 4);
        assert_eq!(round_up_int_div(2, 2), 1);
        assert_eq!(round_up_int_div(0, 2), 0);
        assert_eq!(round_up_int_div(192, 5), 39);
    }
}
