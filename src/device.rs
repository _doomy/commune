use crate::{
    frame::{Frame, MultipartStatus},
    *,
};
use arrayvec::ArrayVec;
use core::convert::TryInto;
use serde::{Deserialize, Serialize};
use serde_cbor::{de::from_mut_slice, ser::SliceWrite, Serializer};

/// Represents a single transceiver
pub struct Device {
    /// The ID of this device
    pub id: u16,
    pub rx: ArrayVec<[Frame; BUFFER_TOTAL_FRAMES]>,
    pub tx: ArrayVec<[Frame; BUFFER_TOTAL_FRAMES]>,
    /// Scratch buffer shared between RX and TX for easier mutations.
    pub scratch: [u8; BUFFER_DATA_TOTAL_SIZE_BYTES],
}

impl Device {
    pub fn new(id: u16) -> Self {
        Self {
            id,
            rx: ArrayVec::<[Frame; BUFFER_TOTAL_FRAMES]>::new(),
            tx: ArrayVec::<[Frame; BUFFER_TOTAL_FRAMES]>::new(),
            scratch: [0; BUFFER_DATA_TOTAL_SIZE_BYTES],
        }
    }

    /// Serialize data to the TX buffer via CBOR in preperation for sending
    /// across a radio or wire. This overwrites anything currently in the TX
    /// buffer without warning.
    ///
    /// # Arguments
    ///
    /// * `rx_id` - The intended receiving ID
    /// * `data` - Arbitrary data to serialize
    ///
    /// # Examples
    ///
    /// ```
    /// # use commune::*;
    /// # use serde::Serialize;
    /// let mut device = Device::new(1);
    ///
    /// // Serialize the number 69 to our TX buffer, targeting another device with the ID of 0.
    /// device.serialize(0, &69);
    ///
    /// // We can also serialize anything implementing serde's Serialize trait.
    /// #[derive(Serialize)]
    /// pub struct MyData<'a> {
    ///     name: &'a str,
    ///     age: usize,
    /// }
    ///
    /// // Serialize this data TX buffer, targeting another device with the ID of 0. Our receiving device
    /// // will need to know beforehand the data structure.
    /// let data = MyData { name: &"Karl", age: 32 };
    /// device.serialize(0, &data);
    /// ```
    pub fn serialize<T>(&mut self, rx_id: u16, data: &T) -> Result<(), Error>
    where
        T: Serialize,
    {
        // Creates a new serializer and serialize data with CBOR into the
        // scratch buffer
        let mut serializer = Serializer::new(SliceWrite::new(&mut self.scratch));
        data.serialize(&mut serializer)
            .map_err(|_e| Error::Cbor("Error serializing"))?;

        let bytes_written = serializer.into_inner().bytes_written();
        let tx_frames_length = round_up_int_div(bytes_written, DATA_SIZE_BYTES);

        // Copy over the serialized data from our scratch buffer, add the
        // correct metadata, and push to our TX queue
        for (i, chunk) in self.scratch[0..bytes_written]
            .chunks(DATA_SIZE_BYTES)
            .enumerate()
        {
            self.tx.push(
                Frame::new(self.id)
                    .set_rx_id(rx_id)
                    .set_multipart({
                        // Set the multipart status as start on the first `Frame`
                        if i == 0 {
                            MultipartStatus::Start(tx_frames_length as u16)
                        } else {
                            MultipartStatus::Continue
                        }
                    })
                    // Dump the bytes we just get into this `Frame`
                    .set_data(chunk),
            );
        }

        Ok(())
    }

    /// We don't keep track of how long serialized data is, and sometimes
    /// we just won't know. We can just find the first null byte, and
    /// that index will be our new length. Returns the usize index upon success.
    fn get_serialized_scratch_data_length(&mut self) -> Option<usize> {
        self.scratch.iter().position(|x| *x == '\0' as u8)
    }

    /// Takes each `Frame` in our RX buffer and places it in
    /// the scratch buffer
    fn convert_rx_buffer_to_scratch_data(&mut self) {
        // First, convert the RX queue into a contiguous array of data in our
        // scratch buffer so we can deserialize it zero our scratch
        self.scratch = [0; BUFFER_DATA_TOTAL_SIZE_BYTES];
        let mut scratch_cur = 0;

        for frame in &mut self.rx {
            let data: [u8; DATA_SIZE_BYTES] = frame.data().try_into().unwrap();
            // write to scratch
            for byte in data.iter() {
                self.scratch[scratch_cur] = *byte;
                scratch_cur += 1;
            }
        }
    }

    /// Takes the contents of the RX buffer and deserializes it to type T.
    pub fn deserialize<'de, T>(&'de mut self) -> Result<T, Error>
    where
        T: Deserialize<'de>,
    {
        self.convert_rx_buffer_to_scratch_data();

        // get the length of the slice needed for deserializing
        let data_length = {
            let d = self.get_serialized_scratch_data_length();
            d.unwrap_or(BUFFER_DATA_TOTAL_SIZE_BYTES)
        };

        let slice = &mut self.scratch[0..data_length];

        // Use CBOR to deserialize and return from our scratch.
        Ok(from_mut_slice::<'de, T>(slice).map_err(|_| Error::DeserializationGeneral)?)
    }
}

#[cfg(test)]
mod tests {
    use crate::tests::TestData;

    use super::*;

    #[test]
    fn create_queue_with_correct_data() {
        let mut device = Device::new(0x1);
        device.serialize(0x0, &TestData::default()).unwrap();

        let test_msgs = TestData::default_frames();
        for (i, frame) in device.tx.iter().enumerate() {
            assert_eq!(test_msgs[i], *frame);
        }
    }

    #[test]
    fn enqueue_a_multipart_message() {
        // virtual transmitter
        let mut device_1 = Device::new(0x1);
        let data = TestData::default();
        device_1.serialize(0x0, &data).unwrap();

        // virtual receiver
        let mut device_2 = Device::new(0x2);
        device_2.rx.push(device_1.tx.pop_at(0).unwrap());
        // Assert that our first message is giving us a start with the correct
        // number of frames
        assert_eq!(
            device_2.rx.pop_at(0).unwrap().multipart(),
            MultipartStatus::Start(4)
        );
    }

    #[test]
    fn receive_correct_data() {
        let mut device_1 = Device::new(0x1);
        let data = TestData::default();
        device_1.serialize::<TestData>(0x2, &data).unwrap();
        assert_ne!(device_1.tx.len(), 0);
        // virtual receiver
        let mut device_2 = Device::new(0x2);
        for frame in device_1.tx {
            device_2.rx.push(frame);
        }

        device_2.convert_rx_buffer_to_scratch_data();
        let result: [u8; 90] = device_2.scratch[0..90].try_into().unwrap();

        assert_eq!(result, TestData::default_serialized());
    }

    #[test]
    fn deserialize_test_data() {
        let mut test_data = TestData::default_serialized().clone();

        // Use CBOR to deserialize and return from our scratch.
        let result = from_mut_slice::<TestData>(&mut test_data)
            .map_err(|_| Error::DeserializationGeneral)
            .unwrap();

        assert_eq!(result, TestData::default());
    }

    #[test]
    fn serialize_and_deserialize_a_message() {
        let mut device_1 = Device::new(0x1);
        let data = TestData::default();
        device_1.serialize::<TestData>(0x2, &data).unwrap();
        assert_ne!(device_1.tx.len(), 0);

        // virtual receiver
        let mut device_2 = Device::new(0x2);
        for frame in device_1.tx {
            device_2.rx.push(frame);
        }
        let result = device_2.deserialize::<TestData>().unwrap();
        assert_eq!(data, result);
    }

    #[test]
    fn create_message_queue_with_too_much_data() {
        let mut msgs = Device::new(0x1);
        let msg = "MPQIQSYGSMGQQAIMEKBUUAHGLZYZOYSKRYVXPNGQGFVTONRSKYVGURXZRAJMZVMWULXVYDKKNWTPSEBEEKEPHFCJTTKHKHNWHMUVLCBSFUVXKRBXLBNUOOERBNUQGHECETOSLXSNXESZLMQTVNNCRMWDQQPVMJISDIRIZTBDLCPAAUZDWFVHDOUOLQZSYPMTKLVZUHYRAKMTLZLWUSHDEJZFQBBTQPQSJTTEEHWSAOMICXKCMUTNHXUZRKMFGTBFHGERQDGWAQVVFNFKQUFXCPVUFNQILMGXJERQWAJTKYZENRQAETMBNTRASIPSJGGWGEANGUTSBGNIHRMGDDLYEQOPUYTZGMITLPAYVQPLTAFDRFESVGMLPZDUFRZILFJURCQRXQTBQTLXCWYEFUJOESSXDETDIUZIDRVHYQPTNTCKZTLYOJCEWCFQANRKYGBRCJWPMICFOLZGMHVCHCBFJCVQMKSDMUUSWAHMGLQJMNZJTIPAQNDTPFRZNXHKKVQDSXUVUPSPLNXQNBUJUBKFZEYSOVNYORSHAMJQTMXCOBLPFGYGCBWVUXXFJGTMLPTDHGMGTJMCSGYJVXBLZQFDAKHVAGFVAOSFEDUASZCYTZTYSCKWCMWYOAZLHALPSSGUDRTDBLJHBOTGXUEZVUQJBYMJPPTFSBCWSSGJRHPSRRMJOGZWKQUHGIGYYFSFAYOOVXECKJYXQKEEAENJNIVWUZKHQZTHLMXFOBUIGSWHGSWNGRHRIZNOFJZMJNEDFPNNEVISJHQEVXAOIEKWNAQAURLFHSOCZVXOWXGKCMJHMNZLIAZZLHIYSJWDGOMPGDNZYBGLHMYTCCRDRMCNNUENZMOUTWZHWXQNCUHDVFJZHMDSYCHPJMPDOFRTVSWGZSTKJZRWTPIXOLZUICJQXCASMANZXLHTMDIYCDOPMBGCHDJMXHCGJQCDJPKFQRHAGMPINGZGWKVWXQDDJIYPWXPTVEDWCTNSAVDWDTRKTGZXCBSNJNGIECWUBULXIPSEABMZMGOWWIIWRLOKYZLVGKSNXOYQKTPXDXBYQMQCXWIMONCLXYEZEOYYBCCGHTKOFLCLGHILCDCKBZFVSVMTEZDVNQVJWXBSEZFBDGYVJAGDGMAUBHNLXOZOEJHTFCMGULYTZKTATFELYUWJMKRFKHLBKTQSRCEHEYUPSKCUBJFMVTBXSYIWUGLNXTSASMCOQVXOGLSBULPMHYWJSVPDDOPADJMARSFNZISDJNHFMVIROFVOCVWZCHOQDJCOBGATPKLVNSPIYVUVBRYBHSRRQWEIIYYDTZDFDOGYMEAMCKIGRFNBKIAIOMQYURIDIZNDMDEZHBNJGMLMYAXBLOPVNNSYQLXCHIXJGWSSWNWGWPEFYVZCCMONGGMXMELJHUYQZDNCRXARARJNPOMDNHRAIWZFDTWMCAUILNCGRUUCXFEHTYROOJMKFNERVCYCUISMIJDSSNVPFAERQJDXRGBFEFTVYGJFCOOFLESZOHIWWNOPVIFLIZVUEHLUWFHYXTCYKTVNRROOKBASMGDOWMKHGGNULGOLCSTGDMHZRKYYHFGQZPZBKQIQYYSIYTGQBJAEDLBR";
        let data = TestData::Text(msg);
        let r = msgs.serialize(0x0, &data);
        assert!(r.is_err());
    }
}
